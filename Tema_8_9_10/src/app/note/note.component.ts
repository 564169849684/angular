import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Note } from '../_models/note';
import { NoteService } from '../_services/note.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit, OnChanges {

  @Input() selectedCategoryId: string = ""
  @Input() searchedTerm: string = ""

  notes: Note[] = []

  constructor(
    private noteService: NoteService,
  ) { }

  deleteNote(id: string): void {
    this.noteService.delete(id);
    this.noteService.getNotes();
  }

  ngOnInit(): void {
    this.noteService.serviceCall();
    this.noteService.getNotes().subscribe(result => {
      this.notes = result;
    });
    console.log(this.notes);
  }

  ngOnChanges(): void {
    if (this.selectedCategoryId) {
      this.noteService.getFilteredNotes(this.selectedCategoryId).subscribe(result => {
        this.notes = result
      });
    }
    if (this.searchedTerm) {
      this.noteService.getSearchedNoteByTitle(this.searchedTerm).subscribe(result => {
        this.notes = result
      })
    } 
  }


}
