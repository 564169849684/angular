import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Note } from '../_models/note';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  // notes: Note[] = [
  //   {
  //     id: "Id1",
  //     title: "First note",
  //     description: "This is the description for the first note",
  //     categoryId: "2"
  //   },
  //   {
  //     id: "Id2",
  //     title: "Second note",
  //     description: "This is the description for the second note",
  //     categoryId: "1"
  //   }
  // ];

  readonly baseUrl= "https://localhost:4200";

  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };


  constructor(
    private httpClient: HttpClient
  ) { }

  serviceCall() {
    console.log("Note service was called");
  }

  getNotes(): Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl+`/notes`, this.httpOptions);
  }


  // getNotes(): Note[] {
  //   return this.notes;
  // }

  checkCategory(category: string): string {
    switch (category) {
      case "To Do": {
        return "1"
      }
      case "Done": {
        return "2"
      }
      case "Doing": {
        return "3"
      }
    }
    return "4"
  }

  addNote(noteTitle:string, noteDescription:string, noteCategoryId:string){
    let note= {  
                description: noteDescription,
                title: noteTitle,
                categoryId: noteCategoryId
                }
    return  this.httpClient.post(this.baseUrl+"/note", note, this.httpOptions).subscribe();
  }

  delete(id: string): any {
    return this.httpClient.delete(this.baseUrl + "/note/" + id).subscribe();
  }


  // addNote(title: string, description: string, category: string): void {
  //   let categoryId = this.checkCategory(category)
  //   this.notes.push({
  //     id: "Id" + String(this.notes.length + 1),
  //     title: title,
  //     description: description,
  //     categoryId: categoryId
  //   })
  // }

  getFilteredNotes(categId: string): Observable<Note[]> {
    return this.httpClient
      .get<Note[]>(
        this.baseUrl + `/notes`,
        this.httpOptions
      )
      .pipe(
        map((notes) => notes.filter((note) => note.categoryId === categId))
      );
  }

  getSearchedNoteByTitle(title: string): Observable<Note[]> {
    return this.httpClient
      .get<Note[]>(
        this.baseUrl + `/notes`,
        this.httpOptions
      )
    .pipe(
      map((notes) => notes.filter((note) => note.title.includes(title)))
    );
}

  // getSearchedNoteByTitle(title: string): Observable<Note[]> {
  //     return this.httpClient
  //       .get<Note[]>(
  //         this.baseUrl + `/notes`,
  //         this.httpOptions
  //       )
  //     .pipe(
  //       map((notes) => notes.filter((note) => note.title.includes(title)))
  //     );
  // }

  // getFilteredNotes(categoryId: string): Note[] {
  //   let auxArray = []
  //   for (let note of this.notes) {
  //     if (note.categoryId == categoryId) {
  //       auxArray.push(note);   
  //     }
  //   }
  //   return auxArray;
  // }
}
