import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriesService } from '../_services/categories.service';
import { NoteService } from '../_services/note.service';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {

  title: string = ""
  description: string = ""
  inputNoteTitle: any
  inputNoteDescription: any

  categories: string[] = []
  selectedValue: string = ""

  constructor(
    private noteService: NoteService,
    private categoryService: CategoriesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.categories = this.getCategory()
  }

  addNote(): void {
    this.noteService.addNote(this.title, this.description, this.selectedValue)
    this.router.navigateByUrl('');
    console.log("selected value: " + this.selectedValue)
  }

  getCategory(): string[] {
    return this.categoryService.getCategories()
  }

}
