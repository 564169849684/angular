import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss']
})
export class WorkspaceComponent implements OnInit {

  lowerString: string = "here is a string"
  upperString: string = "HERE IS ANOTHER STRING"
  pi: number = 3.14159265359

  stringArray: string[] = ['0', '1', '2', '3', '4']
  dateArray: Date[] = [new Date(2000, 1), new Date(2000, 2), new Date(2000, 2), new Date(2000, 3), new Date(2000, 4)]

  show: boolean = true

  constructor() { }

  ngOnInit(): void {
  }

  counter(i: number): any {
    return new Array(i);
  }

}
