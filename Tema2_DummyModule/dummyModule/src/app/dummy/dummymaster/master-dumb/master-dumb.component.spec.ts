import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDumbComponent } from './master-dumb.component';

describe('MasterDumbComponent', () => {
  let component: MasterDumbComponent;
  let fixture: ComponentFixture<MasterDumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MasterDumbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
