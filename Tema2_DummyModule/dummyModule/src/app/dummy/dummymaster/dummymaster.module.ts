import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterDumbComponent } from './master-dumb/master-dumb.component';



@NgModule({
  declarations: [
    MasterDumbComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MasterDumbComponent
  ]
})
export class DummymasterModule { }
