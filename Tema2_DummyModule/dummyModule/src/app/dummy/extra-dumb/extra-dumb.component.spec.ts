import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraDumbComponent } from './extra-dumb.component';

describe('ExtraDumbComponent', () => {
  let component: ExtraDumbComponent;
  let fixture: ComponentFixture<ExtraDumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExtraDumbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
