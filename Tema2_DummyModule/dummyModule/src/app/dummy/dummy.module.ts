import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VeryDumbComponent } from './very-dumb/very-dumb.component';
import { ExtraDumbComponent } from './extra-dumb/extra-dumb.component';
import { DummymasterModule } from './dummymaster/dummymaster.module';



@NgModule({
  declarations: [
    VeryDumbComponent, 
    ExtraDumbComponent
  ],
  imports: [
    CommonModule,
    DummymasterModule
  ],
  exports: [
    VeryDumbComponent, 
    ExtraDumbComponent
  ]
})
export class DummyModule { }
