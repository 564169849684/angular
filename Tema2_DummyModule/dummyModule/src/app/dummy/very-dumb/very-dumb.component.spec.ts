import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VeryDumbComponent } from './very-dumb.component';

describe('VeryDumbComponent', () => {
  let component: VeryDumbComponent;
  let fixture: ComponentFixture<VeryDumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VeryDumbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VeryDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
