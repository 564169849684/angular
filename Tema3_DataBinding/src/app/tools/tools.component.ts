import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {

  title: string = 'TITLE'
  color: string = ''

  constructor() { }

  ngOnInit(): void {
  }

  checkColor(strColor: any): boolean{
    let styleAux: any

    styleAux = new Option().style;
    styleAux.color = strColor;

    return styleAux.color == strColor;
  }


  buttonClick(): void {
    if (this.checkColor(this.color) && (this.color != '')) {
      alert('you picked ' + this.color);
    } else {
      alert('sorry. this is not a color')
    }

  }

}
