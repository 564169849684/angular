import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Curs2Component } from './curs2/curs2.component';



@NgModule({
  declarations: [Curs2Component],
  imports: [
    CommonModule
  ]
})
export class TestModule { }
