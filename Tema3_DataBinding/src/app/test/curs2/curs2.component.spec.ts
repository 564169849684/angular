import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Curs2Component } from './curs2.component';

describe('Curs2Component', () => {
  let component: Curs2Component;
  let fixture: ComponentFixture<Curs2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Curs2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Curs2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
