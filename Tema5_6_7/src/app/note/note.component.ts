import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Note } from '../_models/note';
import { NoteService } from '../_services/note.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit, OnChanges {

  @Input() selectedCategoryId: string = "";

  notes: Note[] = []

  constructor(
    private noteService: NoteService
  ) { }

  ngOnInit(): void {
    this.noteService.serviceCall();
    this.notes = this.noteService.getNotes();
    console.log(this.notes);
  }

  ngOnChanges(): void {
    if (this.selectedCategoryId) {
      this.notes = this.noteService.getFiltredNotes(this.selectedCategoryId);
    }
  }


}
