import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddNoteComponent } from './add-note/add-note.component';
import { HomeComponent } from './home/home.component';
import { ParamDummyComponent } from './param-dummy/param-dummy.component';

const appRoutes:Routes=[
  { path: "", component: HomeComponent, pathMatch: 'full' },
  { path: "addnote", component: AddNoteComponent },
  { path: "param-dummy/:id", component: ParamDummyComponent},
  { path: '**', redirectTo: ''}
]


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
