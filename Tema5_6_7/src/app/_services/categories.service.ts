import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  cateogoryList: string[] = ["To Do", "Done", "Doing"]

  constructor() { }

  getCategories(): string[] {
    return this.cateogoryList
  }
}
