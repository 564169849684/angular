import { Injectable } from '@angular/core';
import { Note } from '../_models/note';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  notes: Note[] = [
    {
      id: "Id1",
      title: "First note",
      description: "This is the description for the first note",
      categoryId: "2"
    },
    {
      id: "Id2",
      title: "Second note",
      description: "This is the description for the second note",
      categoryId: "1"
    }
  ];

  constructor() { }

  serviceCall() {
    console.log("Note service was called");
  }

  getNotes(): Note[] {
    return this.notes;
  }

  checkCategory(category: string): string {
    switch (category) {
      case "To Do": {
        return "1"
      }
      case "Done": {
        return "2"
      }
      case "Doing": {
        return "3"
      }
    }
    return "4"
  }

  addNote(title: string, description: string, category: string): void {
    let categoryId = this.checkCategory(category)
    this.notes.push({
      id: "Id" + String(this.notes.length + 1),
      title: title,
      description: description,
      categoryId: categoryId
    })
  }

  getFiltredNotes(categoryId: string): Note[] {
    let auxArray = []
    for (let note of this.notes) {
      if (note.categoryId == categoryId) {
        auxArray.push(note);   
      }
    }
    return auxArray;
  }
}
