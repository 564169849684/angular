import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-param-dummy',
  templateUrl: './param-dummy.component.html',
  styleUrls: ['./param-dummy.component.scss']
})
export class ParamDummyComponent implements OnInit {

  routeParameter: any

  constructor(
    private activRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activRoute.params.subscribe( parameter => {
      this.routeParameter = parameter
      console.log(this.routeParameter)
    })
  }

}
