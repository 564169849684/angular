import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParamDummyComponent } from './param-dummy.component';

describe('ParamDummyComponent', () => {
  let component: ParamDummyComponent;
  let fixture: ComponentFixture<ParamDummyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParamDummyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParamDummyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
